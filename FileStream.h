#pragma once
#include "OutStream.h"

#define MAX_SIZE_NAME 100

class FileStream : public OutStream
{	
protected:
	FILE* _f;
	char _name[MAX_SIZE_NAME];
public:
	FileStream(char* name);
	~FileStream();

	FileStream& operator<<(const char *str);
	FileStream& operator<<(int num);
	FileStream& operator<<(void(*pf)());
};
