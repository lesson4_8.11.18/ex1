#include "OutStreamEncrypted.h"

OutStreamEncrypted::OutStreamEncrypted(int offset):_offset(offset)
{
}

OutStreamEncrypted::~OutStreamEncrypted()
{
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(const char *str)
{
	
	char* new_str = (char*)malloc(sizeof(char) * strlen(str));
	int i = 0;
	
	for (i = 0; i < strlen(str); i++)
	{
		new_str[i] = (char)((int)(str[i]) + this->_offset);
	}

	new_str[strlen(str)] = '\0';

	(OutStream)(*this) << new_str;
	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(int num)
{
	char c;
	int count = 0;
	int i = 0;
	int save_num = num;
	char* temp;
	char* str = new char[4];
	while (save_num != 0)
	{
		count++;
		save_num /= 10;
	}

	temp = new char[count];

	for (i = 0; i < count; i++)
	{
		temp[i] = (char)((int)('0') + (num % 10) + this->_offset);
		num = num / 10;
	}

	for (i = 0; i < count; i++)
	{
		str[i] = temp[count - 1 - i];
	}

	str[count] = '\0';

	(OutStream)(*this) << str;

	return *this;
}

OutStreamEncrypted& OutStreamEncrypted::operator<<(void(*pf)())
{
	pf();
	return *this;
}

