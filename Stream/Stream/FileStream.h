#pragma once
#include "OutStream.h"

namespace msl{
	#define MAX_SIZE_NAME 100

	class FileStream : public OutStream
	{	
	protected:
		FILE* f;
		char name[MAX_SIZE_NAME];
	public:
		FileStream(char* name);
		~FileStream();

		FileStream& operator<<(const char *str);
		FileStream& operator<<(int num);
		FileStream& operator<<(void(*pf)());
	};
}