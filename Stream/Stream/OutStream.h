#pragma once
#include <stdio.h>
#include <string>

namespace msl
{
	class OutStream
	{
	public:
		OutStream();
		~OutStream();

		OutStream& operator<<(const char *str);
		OutStream& operator<<(int num);
		OutStream& operator<<(void(*pf)());
	};

	void endline();
}