#pragma once

#include "OutStream.h"

class OutStreamEncrypted : public OutStream
{
private:
	int _offset;
public:
	OutStreamEncrypted(int offset);
	~OutStreamEncrypted();

	OutStreamEncrypted& operator<<(const char *str);
	OutStreamEncrypted& operator<<(int num);
	OutStreamEncrypted& operator<<(void(*pf)());
};

