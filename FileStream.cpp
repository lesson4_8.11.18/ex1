#include "FileStream.h"

FileStream::FileStream(char* name){

	strcpy_s(this->_name, name);
	fopen_s(&this->_f, this->_name, "w");
	fclose(this->_f);
	fopen_s(&this->_f, this->_name, "a");
}

FileStream::~FileStream(){
	fclose(this->_f);
}

FileStream& FileStream::operator<<(const char *str)
{
	(OutStream)(*this) << str;
	fprintf(this->_f,"%s",str);
	return *this;
}

FileStream& FileStream::operator<<(int num)
{
	(OutStream)(*this) << num;
	fprintf(this->_f, "%d", num);
	return *this;
}

FileStream& FileStream::operator<<(void(*pf)())
{
	pf();
	fprintf(this->_f, "\n");
	return *this;
}