#include "FileStream.h"

namespace msl {
	FileStream::FileStream(char* name) {

		strcpy_s(this->name, name);
		fopen_s(&this->f, this->name, "a");
		//fclose(f);
		//fopen_s(&this->f, this->name, "a");
	}

	FileStream::~FileStream() {
		fclose(f);
	}

	FileStream& FileStream::operator<<(const char *str)
	{
		(OutStream)(*this) << str;
		fprintf(this->f, "%s", str);
		return *this;
	}

	FileStream& FileStream::operator<<(int num)
	{
		(OutStream)(*this) << num;
		fprintf(this->f, "%d", num);
		return *this;
	}

	FileStream& FileStream::operator<<(void(*pf)())
	{
		pf();
		fprintf(this->f, "\n");
		return *this;
	}
}
