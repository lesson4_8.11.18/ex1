#include "Logger.h"

Logger::Logger()
{
	this->os = *(new OutStream());
	this->_startLine = true;
}

Logger::~Logger()
{
}

void Logger::setStartLine()
{
	static int lineNum = 1;
	if(this->_startLine)
	{
		printf("% d: ", lineNum);
		lineNum++;
	}
	this->_startLine = !this->_startLine;
}

Logger& operator<<(Logger & l, const char * msg)
{
	if (l._startLine)
	{
		printf("LOG");
		l.setStartLine();
	}
	l.os << msg;

	return l;
}

Logger& operator<<(Logger & l, const int num)
{
	if (l._startLine)
	{
		printf("LOG");
		l.setStartLine();
	}
	l.os << num;

	return l;
}

Logger& operator<<(Logger & l, void(*pf)())
{
	pf();
	l.setStartLine();

	return l;
}
